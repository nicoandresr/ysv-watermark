import * as React from 'react'
import ReactDom from 'react-dom'

const size = 800
const watermark = document.images[0]

function App() {
  const img = new Image()
  function onChangeHandler({ target }) {
    const [file] = target.files
    const reader = new FileReader()
    reader.onload = e => {
      img.addEventListener('load', () => {
        const ctx = document.getElementById('Frame').getContext('2d')
        ctx.drawImage(img, 0, 0, size, size)
        ctx.drawImage(watermark, 0, 0, size, size)
      }, false)
      img.src = e.target.result
    }
    reader.readAsDataURL(file)
  }

  function undo() {
    const ctx = document.getElementById('Frame').getContext('2d')
    ctx.clearRect(0, 0, size, size)
    ctx.drawImage(img, 0, 0, size, size)
    ctx.drawImage(watermark, 0, 0, size, size)
  }

  function fillText() {
    undo()
    const ctx = document.getElementById('Frame').getContext('2d')
    const brand = document.getElementById('brand').value
    const offSet = document.getElementById('offSet').value
    ctx.canvas.style.letterSpacing = '9px'
    ctx.fillStyle = '#fff'
    ctx.font = 'small-caps expanded 32px sans-serif'
    ctx.fillText(brand, 70 + Number.parseInt(offSet, 10), 765)
    ctx.stroke()
  }

  return (
    <main>
      <input type="file" id="file" onChange={onChangeHandler} />
      <br/>
      <canvas width={size} height={size} id="Frame"></canvas>
      <br/>
      <input type="text" id="brand" />
      <input type="number" id="offSet" min="0" defaultValue="0" />
      <input type="button" onClick={fillText} value="fill text" />
    </main>
  )
}

ReactDom.render(
  <App />,
  document.getElementById('app')
)
